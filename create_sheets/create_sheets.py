import os
from Google import Create_Service
import datetime

today=datetime.date.today()
formatted_date=datetime.date.strftime(today,"%Y/%m/%d")



def makesheets():

    CLIENT_SECRET_FILE = 'client_secrets.json'
    API_NAME = 'sheets'
    API_VERSION = 'v4'
    SCOPES =['https://www.googleapis.com/auth/spreadsheets']

    service = Create_Service(CLIENT_SECRET_FILE,API_NAME,API_VERSION,SCOPES)


    """"
    blank spreadsheet file
    """
    """
    dict_keys(['spreadsheetId','properties','sheets','spreadsheetUrl'])
    """
    spreadsheets_body = {
        'properties': {
            'title': f'WFH_DS Team_Time Sheet {formatted_date}'

        },
        'sheets':[
            {
                'properties':{
                    'title':'Manik'
                }
            },
            {
                'properties': {
                    'title': 'Tarun'
                }
            },
            {
                'properties':{
                    'title':'Protik'
                }
            },
            {
                'properties': {
                    'title': 'Shambhavi'
                }
            },
            {
                'properties': {
                    'title': 'Shubham Tomar'
                }
            }
        ],

    }
    sheets_body = {
        'properties': {
            'title': f'WFH_Web Team_Time Sheet  {formatted_date}'

        },
        'sheets':[
            {
                'properties':{
                    'title':'Rashika'
                }
            },
            {
                'properties': {
                    'title': 'Data'
                }
            },

        ],

    }
    sheets_file1 =service.spreadsheets().create(body=spreadsheets_body).execute()
    sheets_file2 =service.spreadsheets().create(body=sheets_body).execute()
    return sheets_file1['spreadsheetUrl'],sheets_file2['spreadsheetUrl'],sheets_file1['spreadsheetId'],sheets_file2['spreadsheetId']




##print(sheets_file1['spreadsheetUrl'])
# print(sheets_file2['spreadsheetUrl'])




