import smtplib

import config
import datetime
from updatesheet  import update

today=datetime.date.today()
formatted_date=datetime.date.strftime(today,"%d %B, %Y")

def send_email(subject,msg):
    try:
        server = smtplib.SMTP("smtp.gmail.com",587)
        server.ehlo()
        server.starttls()
        server.login(config.EMAIL_ADDRESS,config.PASSWORD)
        message ="From: %s\r\n" % config.EMAIL_ADDRESS+ "To: %s\r\n" % toaddr+ "CC: %s\r\n" % ",".join(cc)+ "Subject: %s\r\n" % subject+ "\r\n" + msg
        server.sendmail(config.EMAIL_ADDRESS,toaddrs,message)
        server.quit()
        print("Sucess: Email sent")
    except:

        print("Email failed to send")
toaddr ='ayushtyagi604@gmail.com'
cc=['aayushtyagi604@gmail.com']
bcc =['rockingg.ash@gmail.com']
toaddrs= [toaddr] + cc + bcc
subject =f'WFH  {formatted_date}'

# dswork = "https://docs.google.com/spreadsheets/d/19j_MFZnmQMMnN_m5TYj8kibXNJGsRhIufIUqYFPphhE/edit?usp=sharing"
# webwork = "https://docs.google.com/spreadsheets/d/1ao99DpPOzOIzch-FURylwI8-BQIbjsEd3RE4ai6w4gs/edit?usp=sharing"

dswork,webwork, = update()

dsboard = "https://cognitensor.atlassian.net/jira/software/projects/DS/boards/4"
webboard = "https://cognitensor.atlassian.net/jira/software/projects/WEB/boards/11"

msg =f""""Hi Everyone,

Find:
- DS Team Time Sheet - {dswork}
- Web Team Time Sheet - {webwork}

The daily meeting will be run through Jira. Make sure your tasks are all updated and visible on the corresponding board:
- DS Board here - {dsboard}
- Web Board here - {webboard}

If you are on leave, request you to mark the same in your sheet.
Your work log must be completed by 6:30 pm at the latest.
For all documentation tasks, share the corresponding links of the documentation (Confluence preferably, or Google Drive) in the timesheet.

Best Regards,

Assia Mounir
Project Manager
CogniTensor
+91 7428079902"""


send_email(subject,msg)
