import os
from Google import Create_Service
from create_sheets import makesheets
import datetime
today=datetime.date.today()
formatted_date=datetime.date.strftime(today,"%d/%m/%Y")

def update():
    CLIENT_SECRET_FILE = 'client_secrets.json'
    API_NAME = 'sheets'
    API_VERSION = 'v4'
    SCOPES = ['https://www.googleapis.com/auth/spreadsheets']
    dswork_url, webwork_url, dswork_id, webwork_id = makesheets()

    service = Create_Service(CLIENT_SECRET_FILE, API_NAME, API_VERSION, SCOPES)

    spreadsheet_id = webwork_id
    spreadsheet_id2= dswork_id
    myspreadsheets2= service.spreadsheets().get(spreadsheetId= spreadsheet_id2).execute()
    myspreadsheets = service.spreadsheets().get(spreadsheetId = spreadsheet_id).execute()

    worksheet_name='Rashika!'
    cell_range_insert='A1'
    values=[
        ('Name', 	'Rashika Hali'),
        ('Date', 	formatted_date  ),
        ('','','','Time-Sheet',''),
        ('Sr.No.',	'Project', 'Name', 	'Task',	'Start Time',	'End Time',	'Total Hrs Sent',	'Comments'),
        ('1',	'','','','','',				'00:00'),
        ('2',	'','','','','',				'00:00'),
        ('3',	'','','','','',				'00:00'),
        ('4',	'','','','','',				'00:00'),
        ('5',	'','','','','',				'00:00'),
        ('6',	'','','','','',				'00:00'),
        ('Grant Total',	'','','','','',				'00:00'	),
        ('',''),
        ('','Links to documentation')
        ]
    value_range_body={
        'majorDimension' :'ROWS',
        'values':values,

    }


    worksheet_name2='Manik!'
    cell_range_insert2='A1'
    values2=[
        ('Name', 	'Manik'),
        ('Date', 	formatted_date  ),
        ('','','','Time-Sheet',''),
        ('Sr.No.',	'Project', 'Name', 	'Task',	'Start Time',	'End Time',	'Total Hrs Sent',	'Comments'),
        ('1',	'','','','','',				'00:00'),
        ('2',	'','','','','',				'00:00'),
        ('3',	'','','','','',				'00:00'),
        ('4',	'','','','','',				'00:00'),
        ('5',	'','','','','',				'00:00'),
        ('6',	'','','','','',				'00:00'),
        ('Grant Total',	'','','','','',				'00:00'	),
        ('',''),
        ('','Links to documentation')
        ]
    value_range_body2={
        'majorDimension' :'ROWS',
        'values':values2,

    }
    worksheet_name3='Tarun!'
    cell_range_insert3='A1'
    values3=[
        ('Name', 	'Tarun'),
        ('Date', 	formatted_date  ),
        ('','','','Time-Sheet',''),
        ('Sr.No.',	'Project', 'Name', 	'Task',	'Start Time',	'End Time',	'Total Hrs Sent',	'Comments'),
        ('1',	'','','','','',				'00:00'),
        ('2',	'','','','','',				'00:00'),
        ('3',	'','','','','',				'00:00'),
        ('4',	'','','','','',				'00:00'),
        ('5',	'','','','','',				'00:00'),
        ('6',	'','','','','',				'00:00'),
        ('Grant Total',	'','','','','',				'00:00'	),
        ('',''),
        ('','Links to documentation')
        ]
    value_range_body3={
        'majorDimension' :'ROWS',
        'values':values3,

    }
    worksheet_name4='Protik!'
    cell_range_insert4='A1'
    values4=[
        ('Name', 	'Protik'),
        ('Date', 	formatted_date  ),
        ('','','','Time-Sheet',''),
        ('Sr.No.',	'Project', 'Name', 	'Task',	'Start Time',	'End Time',	'Total Hrs Sent',	'Comments'),
        ('1',	'','','','','',				'00:00'),
        ('2',	'','','','','',				'00:00'),
        ('3',	'','','','','',				'00:00'),
        ('4',	'','','','','',				'00:00'),
        ('5',	'','','','','',				'00:00'),
        ('6',	'','','','','',				'00:00'),
        ('Grant Total',	'','','','','',				'00:00'	),
        ('',''),
        ('','Links to documentation')
        ]
    value_range_body4={
        'majorDimension' :'ROWS',
        'values':values4,

    }
    worksheet_name5='Shambhavi!'
    cell_range_insert5='A1'
    values5=[
        ('Name', 	'Shambhavi'),
        ('Date', 	formatted_date  ),
        ('','','','Time-Sheet',''),
        ('Sr.No.',	'Project', 'Name', 	'Task',	'Start Time',	'End Time',	'Total Hrs Sent',	'Comments'),
        ('1',	'','','','','',				'00:00'),
        ('2',	'','','','','',				'00:00'),
        ('3',	'','','','','',				'00:00'),
        ('4',	'','','','','',				'00:00'),
        ('5',	'','','','','',				'00:00'),
        ('6',	'','','','','',				'00:00'),
        ('Grant Total',	'','','','','',				'00:00'	),
        ('',''),
        ('','Links to documentation')
        ]
    value_range_body5={
        'majorDimension' :'ROWS',
        'values':values5,

    }
    worksheet_name6='Shubham Tomar!'
    cell_range_insert6='A1'
    values6=[
        ('Name', 	'Shubham Tomar'),
        ('Date', 	formatted_date  ),
        ('','','','Time-Sheet',''),
        ('Sr.No.',	'Project', 'Name', 	'Task',	'Start Time',	'End Time',	'Total Hrs Sent',	'Comments'),
        ('1',	'','','','','',				'00:00'),
        ('2',	'','','','','',				'00:00'),
        ('3',	'','','','','',				'00:00'),
        ('4',	'','','','','',				'00:00'),
        ('5',	'','','','','',				'00:00'),
        ('6',	'','','','','',				'00:00'),
        ('Grant Total',	'','','','','',				'00:00'	),
        ('',''),
        ('','Links to documentation')
        ]
    value_range_body6={
        'majorDimension' :'ROWS',
        'values':values6,

    }
    service.spreadsheets().values().update(
        spreadsheetId=spreadsheet_id,
        valueInputOption='USER_ENTERED',
        range= worksheet_name + cell_range_insert,
        body=value_range_body
    ).execute()
    service.spreadsheets().values().update(
        spreadsheetId=spreadsheet_id2,
        valueInputOption='USER_ENTERED',
        range= worksheet_name2 + cell_range_insert2,
        body=value_range_body2
    ).execute()
    service.spreadsheets().values().update(
        spreadsheetId=spreadsheet_id2,
        valueInputOption='USER_ENTERED',
        range= worksheet_name3 + cell_range_insert3,
        body=value_range_body3
    ).execute()
    service.spreadsheets().values().update(
        spreadsheetId=spreadsheet_id2,
        valueInputOption='USER_ENTERED',
        range= worksheet_name4 + cell_range_insert4,
        body=value_range_body4
    ).execute()
    service.spreadsheets().values().update(
        spreadsheetId=spreadsheet_id2,
        valueInputOption='USER_ENTERED',
        range= worksheet_name5 + cell_range_insert5,
        body=value_range_body5
    ).execute()
    service.spreadsheets().values().update(
        spreadsheetId=spreadsheet_id2,
        valueInputOption='USER_ENTERED',
        range= worksheet_name6 + cell_range_insert6,
        body=value_range_body6
    ).execute()

    return myspreadsheets2['spreadsheetUrl'],myspreadsheets['spreadsheetUrl']




